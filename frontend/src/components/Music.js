import React from "react";
import PlayMusic from "./PlayMusic";
import RemoveMusic from "./RemoveMusic";

const Music = ({ music }) => {

  const dateParser = (date) => {
    let newDate = new Date(date).toLocaleDateString("en-US", {
      year: "numeric",
      month: "numeric",
      day: "numeric"//,
      // hour: "numeric",
      // minute: "numeric",
    });
    return newDate;
  };

  return (
    <div className="music">
      <div className="music_header">
        <img src={music.snippet.thumbnails.url} alt="video thumbnail"/>
        <div className="title">{music.title}</div>
        <div className="duration_raw">{music.duration_raw}</div>
      </div>
      <div className="btn_container">
        <PlayMusic url={music.url} />
        <RemoveMusic id={music.id.videoId} />      
      </div>
      <div>
        <a href={music.url} target="_blank" rel="noreferrer">{music.url}</a>
        <br />
        <em>Added on {dateParser(music.date)}</em>
      </div>
    </div>
  );
};

export default Music;
