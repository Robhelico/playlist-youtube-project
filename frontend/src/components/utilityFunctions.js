export const urlFormatter = (url) => {
    let index = 0;
    let char = url[0];
    while (char !== '='){
      char = url[index++];
    }
    return("https://youtube.com/embed/" + url.slice(index));
  }