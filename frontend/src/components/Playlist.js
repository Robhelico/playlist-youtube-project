import React, { useEffect, useState } from "react";
import Music from "./Music";
import axios from "axios";

axios.defaults.headers.common = {
  "Content-Type": "application/json"
}

const Playlist = () => {
  //initialisation
  const [playlist, setPlaylist] = useState([]);
  const [search, setSearch] = useState("");
  const [error, setError] = useState(false);
  const [playerUrl, setPlayerUrl] = useState ("");
  const port = 8000;

  useEffect(() => { //refresh the values
    getPlaylist();
    getPlayerURL();
  }, []);

  const getPlaylist = () => { //get the playlist from localstorage, key = "musics"

    const currentPlaylist = localStorage.getItem("musics");

    if(currentPlaylist !== '' && currentPlaylist !== null){//if playlist from browser not empty, get it
      const myJSON = JSON.parse(currentPlaylist);
      setPlaylist(myJSON);
    }
    else{//else, create new playlist
      localStorage.setItem("musics",'[]');
    }  
  };

  const getPlayerURL = _ => {
    const currentUrl = localStorage.getItem("current_url");

    if(currentUrl !== '' && currentUrl !== null){//if url from browser not empty, get it
      const myJSON = JSON.parse(currentUrl);
      setPlayerUrl(myJSON);
    }
    else{//else, create new playlist
      localStorage.setItem("current_url",'""');
    }  
  };

  const handleSubmit = (e) => {
    e.preventDefault();//avoid the page to refresh

    if (search) {
      let first_result = {};
      // const data_sent = {"params":{yt_search : search}}; //get METHOD
      const data_sent = {yt_search : search};

      axios //ask information about the search to the endpoint
        // .get("http://localhost:"+port+"/", data_sent) //get METHOD
        .post(
          "http://localhost:"+port+"/",
          data_sent, 
          {headers: { 'Content-type': 'application/json; charset=UTF-8' }}
        ) //post METHOD
        .then((res) => {
          first_result = res.data;

          if (!first_result) { //no result for Youtube search
            setError(true);
          } 
          else { //store the music in the playlist
            first_result["date"] = Date.now();
            const newPlaylist = [...playlist, first_result];
            const myJSON = JSON.stringify(newPlaylist);
            localStorage.setItem ("musics", myJSON);
            setError(false);
            setSearch("");
            getPlaylist();  
          }
        })
        .catch(error => {
          console.log(error)});
    }
    else {
      setError(true);
    }
  };

  return (
    <div>
      <div className="searchspace">
        <div className="big_title">
          <img src="./images/youtube_logo.png" id="logo" alt="Youtube Logo" />
        </div>  

        <form onSubmit={(e) => handleSubmit(e)} className="searchbar">
          <input
            onChange={(e) => setSearch(e.target.value)}
            type="text"
            placeholder="Your new Youtube video search"
            value={search}
            className="textbar"
            autoFocus
            style={{ background: error ? "#ffb3b3" : "#ffdcdc" }}
          />
          {error && <p>No result !</p>}
          <input type="submit" className="submit" value="Add music" />
        </form>
      </div>

      <div className="player_container">
        <iframe 
        title="video_player" 
        height="400" width="600" allowFullScreen 
        src={playerUrl}>
        </iframe>
      </div>
      
      <div className="playlist">
        <ul>
          {playlist
            .sort((a, b) => b.date - a.date)
            .map((music) => {
              return (<Music key={music.id.videoId} music={music} />);
            })}
        </ul>
      </div>
      
    </div>
  );
};

export default Playlist;

/*** DATA STRUCTURE ***/
// {
// "id": {
//   "videoId": "0E6_S3XkArs"
// },
// "url": "https://www.youtube.com/watch?v=0E6_S3XkArs",
// "title": "Générique - Les Bobodioufs",
// "description": "",
// "duration_raw": "1:00",
// "snippet": {
//   "url": "https://www.youtube.com/watch?v=0E6_S3XkArs",
//   "duration": "1:00",
//   "publishedAt": "7 years ago",
//   "thumbnails": {
//       "id": "0E6_S3XkArs",
//       "url": "https://i.ytimg.com/vi/0E6_S3XkArs/hqdefault.jpg?sqp=-oaymwEjCOADEI4CSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLCCUUPGvXXQpWcai7pUsBGk_RLeng",
//       "default": {
//           "url": "https://i.ytimg.com/vi/0E6_S3XkArs/hqdefault.jpg?sqp=-oaymwEjCOADEI4CSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLCCUUPGvXXQpWcai7pUsBGk_RLeng",
//           "width": 480,
//           "height": 270
//       },
//       "high": {
//           "url": "https://i.ytimg.com/vi/0E6_S3XkArs/hqdefault.jpg?sqp=-oaymwEjCOADEI4CSFryq4qpAxUIARUAAAAAGAElAADIQj0AgKJDeAE=&rs=AOn4CLCCUUPGvXXQpWcai7pUsBGk_RLeng",
//           "width": 480,
//           "height": 270
//       },
//       "height": 270,
//       "width": 480
//   },
//   "title": "Générique - Les Bobodioufs",
//   "views": "39086"
// },
// "views": "39086"
// }
