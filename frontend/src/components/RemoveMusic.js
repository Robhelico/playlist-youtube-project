import React from "react";
import { urlFormatter } from "./utilityFunctions";

const RemoveMusic = ({ id }) => {
  
  const handleRemove = () => {
    const getJSON = localStorage.getItem("musics"); //getting playlist from localStorage
    let currentPlaylist = JSON.parse(getJSON);

    let ptId = currentPlaylist[0].id.videoId; //finding the index of the music that has to be removed
    let index = 0;

    while (ptId !== id){
      index++;
      ptId = currentPlaylist[index].id.videoId;
    }
    const getJSON_2 = localStorage.getItem("current_url"); //if the music removed is displayed in the player, display nothing
    const currentUrl = JSON.parse(getJSON_2);

    if(urlFormatter(currentPlaylist[index].url)  === currentUrl) {
      localStorage.setItem("current_url", '""');
      console.log("\n current_url set to ''");
    }

    currentPlaylist.splice(index, 1); //music removed and new playlist stored in localStorage
    const sendJSON = JSON.stringify(currentPlaylist);
    localStorage.setItem("musics", sendJSON);

    window.location.reload();
  };

  return (
    <button
      onClick={() => {
        if (window.confirm("Remove this music from the playlist ?")) {
          handleRemove();
        }
      }}
      className="remove_button"
    >
      ✖
    </button>
  );
};

export default RemoveMusic;
