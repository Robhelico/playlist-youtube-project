import React from "react";
import { urlFormatter } from "./utilityFunctions";

const PlayMusic = ({ url }) => {
 
  const handlePlay = () => {

    console.log(url);
    const formatted_url = urlFormatter(url);
    const myJSON = JSON.stringify(formatted_url);
    localStorage.setItem("current_url", myJSON);

    window.location.reload();
  };

  return (
    <button
      onClick={() => {handlePlay();}}
      className="play_button"
    >
      ►
    </button>
  );
};

export default PlayMusic;
