import React from 'react';

const NotFound = () => {
    return (
        <div className="not_found">
            <h1>Error 404</h1>
            <h2>Not Found</h2>
        </div>
    );
};

export default NotFound;