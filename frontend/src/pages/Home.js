import React from "react";
import Playlist from "../components/Playlist";

const Home = () => {
  return (
    <div className="home">
      <Playlist/>
    </div>
  );
};

export default Home;
