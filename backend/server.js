const express = require('express');
const yt = require('youtube-search-without-api-key');
const cors = require("cors");



// //res.header("Access-Control-Allow-Origin", "*");
// const corsOptions ={
//    origin:'*', 
//    credentials:true,            //access-control-allow-credentials:true
//    optionSuccessStatus:200,
// }

const app = express();
const port = process.env.PORT || 5000;

app.use(cors(/*corsOptions*/)) // Use this after the variable declaration
app.options('*', cors());
app.use(express.json());

// app.get('/', (req,res) => {
//     res.status(200);
//     res.send("hello")
// });

app.post('/', async (req,res) => {
    console.log("beginning");
    console.log("Query received. About to check YouTube")
    const yt_search =  req.body;
    console.log(yt_search);
    const videos = await yt.search(yt_search.yt_search);
    res.status(200);
    res.send(videos[0]);
    console.log("end");

});

app.get('/', (req,res) => {
    res.status(200);
    res.send({"express": "YOUR EXPRESS BACKEND IS CONNECTED TO REACT"});
});

// app.get('/', async (req,res) => {
//     console.log("beginning");
//     console.log("Query received. About to check YouTube")
//     const yt_search =  req.query.yt_search;
//     console.log(yt_search);
//     const videos = await yt.search(yt_search);
//     res.send(videos[0]);
//     console.log("end");

// });

app.listen(port, () => {
    console.log("Backend endpoint listening at port " + port);
});